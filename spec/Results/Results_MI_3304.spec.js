/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3304.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('non-existing dob: MI-3304', function () {
        minerva.results.btnDropdown.click();
        minerva.results.monthDropdown.sendKeys(testData.month);
        minerva.results.Inputday.clear()
        minerva.results.Inputyear.clear()
        minerva.results.Inputday.sendKeys(testData.day);
        minerva.results.Inputyear.sendKeys(testData.year)
        minerva.results.patientTextAdvance.click();
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual(testData.txtKeyword)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultFound)
	});
    afterEach(function () {
        minerva.logout();
	});
});