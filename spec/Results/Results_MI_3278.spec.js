
   var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3278.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('advance search UI ID: MI-3278', function () {
        minerva.results.btnDropdown.click();
        expect(minerva.results.advanceSearchHeader.getText(),testData.advanceSearchHeader).toBeTruthy();
        expect(minerva.results.patientTextAdvance.getText()).toEqual(testData.patientTextAdvance);
        expect(minerva.results.patientNameTextAdvance.getText()).toEqual(testData.patientNameTextAdvance);
        expect(minerva.results.patientIdTextAdvance.getText()).toEqual(testData.patientIdTextAdvance)
        expect(minerva.results.patientDobTextAdvance.getText()).toEqual(testData.patientDobTextAdvance);
        expect(minerva.results.patientGenderTextAdvance.getText()).toEqual(testData.patientGenderTextAdvance);
        expect(minerva.results.orderNameTextAdvance.getText()).toEqual(testData.orderNameTextAdvance)
        expect(minerva.results.orderDateFromTextAdvance.getText()).toEqual(testData.orderDateFromTextAdvance)
        expect(minerva.results.statusTextAdvance.getText()).toEqual(testData.statusTextAdvance);
        expect(minerva.results.recordTypeTextAdvance.getText()).toEqual(testData.recordTypeTextAdvance);
        expect(minerva.results.orderIdTextAdvance.getText()).toEqual(testData.orderIdTextAdvance)
        expect(minerva.results.orderDateToTextAdvance.getText()).toEqual(testData.orderDateToTextAdvance)
        expect(minerva.results.modalityTextAdvance.getText()).toEqual(testData.modalityTextAdvance);
        expect(minerva.results.orderingPhysicianTextAdvance.getText()).toEqual(testData.orderingPhysicianTextAdvance);
        expect(minerva.results.performingLocationTextAdvance.getText()).toEqual(testData.performingLocationTextAdvance);
        expect(minerva.results.uploadedByTextAdvance.getText()).toEqual(testData.uploadedByTextAdvance);
    //    expect(minerva.results.textBulkUpload.getText()).toEqual('Uploaded from bulk uploader');
        expect(minerva.results.btnCrossAdvanceSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputPatientNameAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputPatientIdAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputOrderIdAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputOrderDateFrom.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputOrderNameAdvance.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputOrderDateTo.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputPerformingLoaction.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputOrderingPhysician.isDisplayed()).toBeTruthy();
        expect(minerva.results.InputUploadedBy.isDisplayed()).toBeTruthy();
        expect(minerva.results.Inputday.isDisplayed()).toBeTruthy();
        expect(minerva.results.monthDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.Inputyear.isDisplayed()).toBeTruthy();
        expect(minerva.results.modalityDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.genderDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.recordTypeDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.statusDropdown.isDisplayed()).toBeTruthy();
        expect(minerva.results.btnClearAll.isDisplayed()).toBeTruthy();
        expect(minerva.results.btnAdvanceSearch.isDisplayed()).toBeTruthy();
        expect(minerva.results.checkboxBulkUpload.isDisplayed()).toBeTruthy();
//resultsf text identifier not  found
	});
    afterEach(function () {
        minerva.logout();
	});
});