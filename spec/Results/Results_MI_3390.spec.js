/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3390.json');

describe('results table', function() {
    beforeEach(function () {
         minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('pagination ID: MI-3390', function () {
        expect(minerva.results.btnPreviousPage.isDisabled()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - 10');
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.totalCount);
        minerva.results.gotoNextPage();
        expect(minerva.results.getCurrentResultsRange()).toEqual('11 - 20');
        minerva.results.gotoNextPage(2);
        expect(minerva.results.getCurrentResultsRange()).toEqual('31 - 40');
        minerva.results.gotoPreviousPage();
        expect(minerva.results.getCurrentResultsRange()).toEqual('21 - 30');
        minerva.results.gotoPreviousPage(2);
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - 10');
    });

    afterEach(function () {
        minerva.logout();
    });
});