/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3297.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('only date/month in dob: MI-3297', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientIdAdvance.clear();
        minerva.results.monthDropdown.sendKeys(testData.month);
        minerva.results.Inputday.sendKeys(testData.day);
        minerva.results.patientTextAdvance.click();
        expect(minerva.results.errorText.getText()).toEqual(testData.notValidDate)
        //expect(minerva.results.btnIsDisabledFromElement(minerva.results.btnAdvanceSearch)).toBeTruthy()
    });

    afterEach(function () {
        minerva.logout();
	});
});