/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3290.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search by non-existing data ID: MI-3290', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientNameAdvance.sendKeys(testData.nonExistingData)
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient Name: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultsText)
    });

    afterEach(function () {
        minerva.logout();
	});
});