/**
 * Created by aayushi on 9/30/2016.
 */

var minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-3556 ways to click Rad icon', function () {
        minerva.patients.patientsTable.row(1).then(function (rowTwo) {
            rowTwo.patientDetails().click();
            minerva.navbar.innerTab('Results').then(function (resultsTab) {
                resultsTab.visit();
            });
            expect(minerva.results.txtSearch.isDisplayed()).toBeTruthy();
            browser.navigate().back();
            browser.navigate().back();
        });

        minerva.patients.patientsTable.row(1).then(function (rowTwo) {
            rowTwo.actionsRadiology().click();
            expect(minerva.results.txtSearch.isDisplayed()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});