/**
 * Created by aayushi on 9/30/2016.
 */

var _ = require('lodash'),
    testData=require('./Patients_MI_4500.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4500 landing on patient view tab', function () {
        expect(minerva.patients.patientsTable.title.getText()).toEqual(testData.tabTitle);
        minerva.patients.btnSearch.click();
        expect(_.keys(columns)).toEqual(testData.columns);
        expect(columns['Gender'].data()).toEqual(testData.gender);
    });

    afterEach(function () {
        minerva.logout();
    });
});