/**
 * Self Test to check the framework itself
 *
 * Created by sujata on 9/1/2016.
 * Modified by ashish on 9/5/2016.
 */

var _ = require('lodash'),
    dashboards= require('../pages/dashboards/dashboards'),
    navbar = require('../pages/navbar'),
    loginPage       = require('../pages/login');

describe('Self Tests: Dashboards table, Inner Tabs', function () {
    var innerTabs;
    beforeAll(function () {
        loginPage.goLogin();
        navbar.tab('Dashboards').then(function (dashboardsTab) {
            dashboardsTab.visit();
        });
        dashboards.dashboardsTable.row(3).then(function (rowTwo) {
            rowTwo.btnAction().click();
        });
        navbar.innerTabs.then(function (baseTabs) {
            innerTabs = baseTabs;
        });
    });

    afterAll(function () {
        navbar.logout();
        loginPage.isLoggedOut();
    });

    it('Should display eleven inner tabs', function () {
        expect(_.size(innerTabs)).toEqual(11);
    });

    /*MNV-5730
    it('should navigate to Family History tab', function () {
        navbar.innerTab('Family History').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });

    it('should navigate to Documents tab', function () {
        navbar.innerTab('Documents').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });

    it('should navigate to Overview tab', function () {
        navbar.innerTab('Overview').then(function (overViewTab) {
            overViewTab.visit();
            expect(overViewTab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Results tab', function () {
        navbar.innerTab('Results').then(function (recordsTab) {
            recordsTab.visit();
            expect(recordsTab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to History tab', function () {
        navbar.innerTab('History').then(function (historyTab) {
            historyTab.visit();
            expect(historyTab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Medications tab', function () {
        navbar.innerTab('Family Members').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Conditions tab', function () {
        navbar.innerTab('Conditions').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Allergies tab', function () {
        navbar.innerTab('Allergies').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Patient Profile tab', function () {
        navbar.innerTab('Patient Profile').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });

    it('should navigate to Observations tab', function () {
        navbar.innerTab('Observations').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });
    it('should navigate to Questionnaire tab', function () {
        navbar.innerTab('Questionnaire').then(function (tab) {
            tab.visit();
            expect(tab.isActive()).toBeTruthy();
        });
    });*/

});
