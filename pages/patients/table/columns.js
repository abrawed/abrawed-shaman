/**
 * Created by aayushi on 9/21/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows.js');

module.exports = Page.create({

    all: {
        get: function () {
            var page = this;
            var columns = {};
            var tblColumnHeadings= ["patientDetails","phoneEmail","visitedDate","actionsOverview","actionsRadiology","actionsLabs","Id","DOB","Gender"]
            tblColumnHeadings.forEach(function(headingElement){
                columns[headingElement]=page.byName(headingElement)
            });
            return columns;
        }
    },

    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },

    columnDataFromName: {
        value: function (columnName) {
            switch (columnName) {
                case "patientDetails":
                    return this.patientDetails;
                case "phoneEmail":
                    return this.phoneEmail;
                case "visitedDate":
                    return this.visitedDate;
                case "actionsOverview":
                    return this.actionsOverview;
                case "actionsRadiology":
                    return this.actionsRadiology;
                case "actionsLabs":
                    return this.actionsLabs;
                case "Id":
                    return this.Id;
                case "DOB":
                    return this.DOB;
                case "Gender":
                    return this.Gender;
                default:
                    this.NoSuchColumnException.thro(columnHeading);
            }
        }
    },

    patientDetails: {
        get: function () {
            var Description = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowPatientNameDetailsFromElement(rowElement).then(function (description) {
                        Description.push(description);
                    });
                });
                return Description;
            });
        }
    },

    phoneEmail: {
        get: function () {
            var Type = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowPhoneEmailFromElement(rowElement).then(function (type) {
                        Type.push(type);

                    });
                });
                return Type;
            });
        }
    },

    visitedDate: {
        get: function () {
            var User = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLastVisitedDateFromElement(rowElement).then(function (user) {
                        User.push(user);

                    });
                });
                return User;
            });
        }
    },

    actionsOverview: {
        get: function () {
            var Context = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowActionsOverviewFromElement(rowElement).then(function (context) {
                        Context.push(context);

                    });
                });
                return Context;
            });
        }
    },

    actionsLabs: {
        get: function () {
            var Context = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowActionsLabsFromElement(rowElement).then(function (context) {
                        Context.push(context);

                    });
                });
                return Context;
            });
        }
    },

    actionsRadiology: {
        get: function () {
            var Context = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowActionsRadiologyFromElement(rowElement).then(function (context) {
                        Context.push(context);

                    });
                });
                return Context;
            });
        }
    },

    Id: {
        get: function () {
            var LastUpdatedBy = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowIdFromElement(rowElement).then(function (lastUpdatedBy) {
                        LastUpdatedBy.push(lastUpdatedBy);

                    });
                });
                return LastUpdatedBy;
            });
        }
    },

    DOB: {
        get: function () {
            var Actions = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowDOBFromElement(rowElement).then(function (actions) {
                        Actions.push(actions);

                    });
                });
                return Actions;
            });
        }
    },

    Gender: {
        get: function () {
            var Actions = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowGenderFromElement(rowElement).then(function (actions) {
                        Actions.push(actions);
                    });
                });
                return Actions;
            });
        }
    },

    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});

